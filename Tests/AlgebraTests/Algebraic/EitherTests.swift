import Algebra
import XCTest

final class EitherTests: XCTestCase {
    func testLeft() {
        // given
        let value = "left-value"
        // when
        let either: Either<String, String> = .left(value)
        // then
        XCTAssertTrue(either.isLeft)
        XCTAssertNil(either.right)
        XCTAssertEqual(value, either.left)
    }

    func testRight() {
        // given
        let value = "right-value"
        // when
        let either: Either<String, String> = .right(value)
        // then
        XCTAssertTrue(either.isRight)
        XCTAssertNil(either.left)
        XCTAssertEqual(value, either.right)
    }
}
