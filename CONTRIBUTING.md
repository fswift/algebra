# CONTRIBUTING

## Development Environment

### xcode

- Version 12.2 (12B45b)

### swift

- Apple Swift version 5.3.1 (swiftlang-1200.0.41 clang-1200.0.32.8)
- Target: x86_64-apple-darwin20.1.0

### macos

- macOS Big Sur - Version 11.0.1

### bundler

The project is using **bundler** for handling the ruby dependencies.

Make sure you have **bundler v2.1.4** installed - this is the version the project is packaged with.

To install the project dependencies:

```shell
bundle install
```

### git-hooks

Make sure you are setting the git-hooks up locally.

```shell
ruby copy-hooks.rb
```
