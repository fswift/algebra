// MARK: - Either

/// Represents a value of one of two possible types (a disjoint union).
/// A type of `Either` is either a value of `Left` or `Right`.
///
/// A common use of `Either` is as an alternative to `Result`.
/// The main difference between them is that the `Result` type constraints its "right side" (`.failure`) to only allow `Swift.Error` conformances.
///
/// When using `Either`, there is the freedom of using both sides of the enum.
/// A projection can be used to selectively operate on a value of type `Either`, depending on whether it is of type `Left` or `Right`.
public enum Either<Left, Right> {
    /// The "left side" of the *Sum* (disjoint union).
    case left(Left)
    /// The "right side" of the *Sum* (disjoint union).
    case right(Right)
}

public extension Either {
    /// Returns `true` if this is a `Left`, `false` otherwise.
    var isLeft: Bool {
        left != nil
    }

    /// Projects this `Either` as a `Left?`.
    var left: Left? {
        if case .left(let value) = self {
            return value
        }
        return nil
    }

    /// Returns `true` if this is a `Right`, `false` otherwise.
    var isRight: Bool {
        right != nil
    }

    /// Projects this `Either` as a `Right?`.
    var right: Right? {
        if case .right(let value) = self {
            return value
        }
        return nil
    }
}
